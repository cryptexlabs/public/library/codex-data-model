import { MessageContextInterface } from "./message-context.interface";

export class MessageContext implements MessageContextInterface {
  constructor(
    public category: string = "default",
    public id: string = "none"
  ) {}
}
