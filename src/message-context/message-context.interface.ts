export interface MessageContextInterface {
  /**
   * Category for context. Default should be "default" if not provided.
   * Every endpoint must be able to receive and pass along this property
   */
  category: "default" | string;

  /**
   * Optional unique identifier for the context. Default should be "none" if not provided.
   * Every endpoint must be able to receive and pass along this property
   * Format is UUID V4
   */
  id: "none" | string;
}
