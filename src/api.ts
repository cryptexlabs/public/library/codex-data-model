import { CountryLanguageCombination, LanguageCode } from "./locale";

export enum ApiOrderDirectionEnum {
  ASC = "asc",
  DESC = "desc",
}

export interface ApiQueryOptionsInterface {
  searchName?: string;
  search?: string;
  orderBy?: string;
  orderDirection?: ApiOrderDirectionEnum;
}

export interface ApiMetaHeadersInterface {
  /**
   * Unique identifier generated when a person takes an action or a system time based event is triggered
   * This correlation id should be passed between systems and should be only created if none exists
   * Stops being re-used when an action is required from a person
   * Format is UUID V4
   */
  "x-correlation-id": string;

  /**
   * ISO 639 alpha-1 language code
   * Every endpoint must be able to receive and pass along this property
   */
  "accept-language": CountryLanguageCombination;

  /**
   * UTC timestamp in ISO 8601 format for when the correlation id was created
   * This timestamp should be passed between systems and should be only created if none exists
   */
  "x-started": string;

  /**
   * UTC timestamp in ISO 8601 format for when this particular message was created
   */
  "x-created": string;

  /**
   * A category for the context of the request. For example 'test', 'performance test' or 'debug'
   */
  "x-context-category": string;

  /**
   * A unique context identifier used for correlating logs and metrics usually for performance or experimental testing
   * UUID v4 format
   */
  "x-context-id": string;

  /**
   * A unique identifier for the client to help identify exactly which application or third party is making the requests
   */
  "x-client-id": string;

  /**
   * Canonical name of the client that is making the API call. Such as "BlueBerry Picker App"
   */
  "x-client-name": string;

  /**
   * Version of the client that is making the API call. Such as '1.3.8'
   */
  "x-client-version": string;

  /**
   * Variation of the client build. There can be different builds of the same version of a client such as 'dev', 'test' or 'demo'
   * Typically these variations would be configured to interact with different backends and have different debugging levels
   */
  "x-client-variant": string;

  /**
   * Optional: An override to set the servers understanding of when is now. Used for end-to-end testing.
   */
  "x-now"?: string;
}
