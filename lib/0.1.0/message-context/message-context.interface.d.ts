export interface MessageContextInterface {
    category: "default" | string;
    id: "none" | string;
}
