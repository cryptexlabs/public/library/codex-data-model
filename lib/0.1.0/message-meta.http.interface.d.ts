import { MessageMetaInterface } from "./message-meta.interface";
export interface MessageMetaHttpInterface extends MessageMetaInterface {
    path?: string;
}
