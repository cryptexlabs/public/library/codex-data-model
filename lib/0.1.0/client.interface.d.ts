export declare class ClientInterface {
    id: string;
    version: string;
    name: string;
    variant: string;
}
