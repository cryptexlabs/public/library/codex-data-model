"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CodexMetaTypeEnum = void 0;
var CodexMetaTypeEnum;
(function (CodexMetaTypeEnum) {
    CodexMetaTypeEnum["NA_HTTP_ERROR"] = "cryptexlabs.codex.http.error";
    CodexMetaTypeEnum["NA_HTTP_SIMPLE"] = "cryptexlabs.codex.http.simple";
    CodexMetaTypeEnum["NA_HTTP_HEALTHZ"] = "cryptexlabs.codex.http.healthz";
    CodexMetaTypeEnum["NA_WEBSOCKET_ERROR"] = "cryptexlabs.codex.websocket.error";
    CodexMetaTypeEnum["NA_WEBSOCKET_SIMPLE"] = "cryptexlabs.codex.websocket.simple";
    CodexMetaTypeEnum["NA_WEBSOCKET_SUBSCRIPTIONS_UPDATED"] = "cryptexlabs.codex.websocket.subscriptions.updated";
    CodexMetaTypeEnum["NA_WEBSOCKET_PING"] = "cryptexlabs.codex.websocket.ping";
})(CodexMetaTypeEnum = exports.CodexMetaTypeEnum || (exports.CodexMetaTypeEnum = {}));
